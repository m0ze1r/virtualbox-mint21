#!/usr/bin/bash
#
echo '---------------------'
echo 'Updating the system'
echo '---------------------'
sleep 2
sudo apt update && sudo apt upgrade
sleep 2
echo '---------------------'
echo 'grabbing dependencies'
echo '---------------------'
sleep 1
sudo apt install wget apt-transport-https gnupg2
echo '---------------------'
echo 'importing virtualbox keystrings'
echo '---------------------'
sleep 2
sudo wget -O- https://www.virtualbox.org/download/oracle_vbox_2016.asc | gpg --dearmor | sudo tee /usr/share/keyrings/virtualbox.gpg
echo '---------------------'
echo 'importing virtualbox into /etc/apt/sources.list'
echo '---------------------'
#
sleep 2
echo deb [arch=amd64 signed-by=/usr/share/keyrings/virtualbox.gpg] https://download.virtualbox.org/virtualbox/debian jammy contrib | sudo tee /etc/apt/sources.list.d/virtualbox.list
#
#
#
echo '---------------------'
echo 'installing virtualbox 6.1'
echo '---------------------'
sudo apt install virtualbox-6.1
echo '---------------------'
echo 'install completed'
echo '---------------------'